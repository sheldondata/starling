# Generated by Django 2.0 on 2018-01-13 17:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0028_tweetmod'),
    ]

    operations = [
        migrations.CreateModel(
            name='TweetClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('val', models.BooleanField()),
                ('reaction', models.ManyToManyField(to='Sententio.Reaction')),
                ('tweet', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Sententio.Tweet')),
            ],
        ),
    ]
