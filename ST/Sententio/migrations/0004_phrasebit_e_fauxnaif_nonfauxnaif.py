# Generated by Django 2.0 on 2018-01-04 02:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0003_auto_20180103_2135'),
    ]

    operations = [
        migrations.AddField(
            model_name='phrasebit',
            name='e_fauxnaif_nonfauxnaif',
            field=models.FloatField(default=0),
        ),
    ]
