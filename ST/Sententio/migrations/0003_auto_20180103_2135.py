# Generated by Django 2.0 on 2018-01-04 02:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0002_remove_target_name_userid'),
    ]

    operations = [
        migrations.AddField(
            model_name='target_tweet',
            name='text',
            field=models.CharField(max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='tweet',
            name='text',
            field=models.CharField(max_length=300, null=True),
        ),
    ]
