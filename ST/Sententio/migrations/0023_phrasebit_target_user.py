# Generated by Django 2.0 on 2018-01-12 19:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0022_phrasebit'),
    ]

    operations = [
        migrations.AddField(
            model_name='phrasebit',
            name='target_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Sententio.Target_Name'),
        ),
    ]
