# Generated by Django 2.0 on 2018-01-08 01:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0009_auto_20180105_1146'),
    ]

    operations = [
        migrations.AddField(
            model_name='tweet',
            name='prcnt_pb',
            field=models.FloatField(default=0),
        ),
    ]
