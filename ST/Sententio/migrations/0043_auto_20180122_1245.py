# Generated by Django 2.0 on 2018-01-22 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sententio', '0042_auto_20180114_1500'),
    ]

    operations = [
        migrations.RenameField(
            model_name='target_tweet',
            old_name='reaction_prcnt',
            new_name='reaction_prcnt_all',
        ),
        migrations.AddField(
            model_name='target_tweet',
            name='reaction_prcnt_tweet',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='target_tweet',
            name='reaction_prcnt_user',
            field=models.FloatField(default=0),
        ),
    ]
