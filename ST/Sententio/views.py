from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
import random
import Sententio
import numpy as np
import json

from django.utils.safestring import SafeData, SafeText, mark_safe

from Sententio.models import Target_Name, Target_Tweet, Tweet, TweetMod, TweetClass, PhraseBit, Reaction, Target_Tweet_Stat

from django.utils.safestring import SafeString

from django.shortcuts import redirect

#Left off here -> need to delete the TweetModeledUnclassed once the TweetModClas is defined
# and then pop up with "no more tweets to classify" <-- this is only if there are none auto-classified (modeled) tweets

from django.template.defaulttags import register

from django.contrib.auth.models import User, Group

@register.filter
def get_all(dictionary, key):
	#splitkey = key.split('.')
	subdict = dictionary.get(key)
	return subdict.get('all')

@register.filter
def get_item(dictionary, key):
	splitkey = key.split('.')
	subdict = dictionary.get(splitkey[0])
	return subdict.get(splitkey[1])

def Classifier_Portal(request):

	print(request)

	try:
		tweetid = request.POST.get("tweetid", "")
		print(request.POST.get("tweetid"))
	except:
		print(request.POST)		

	reactions = Reaction.objects.filter(polarity="POS")
	
	reactset = []
	reactset1 = []
	reactset2 = []

	reactsetflat = []

	for reaction in reactions:
		reactblock = [reaction.name,reaction.opposite.name]
		reactset1.append(reactblock)
	
	reactions = Reaction.objects.filter(polarity="NEU")

	for reaction in reactions:
		reactblock = [reaction.name,reaction.opposite.name]
		reactset2.append(reactblock)

	for reactblock in reactset2:
		if [reactblock[1],reactblock[0]] in reactset2:
			reactset2.remove(reactblock)

	reactset.extend(reactset1)
	reactset.extend(reactset2)

	reactions = Reaction.objects.all()

	for reaction in reactions:
		reactsetflat.append(reaction.name)

	def grab_random():
		global randomtweet
		global index1

		try:
			last = Tweet.objects.filter(classified=False).count() - 1
			index1 = random.randint(0, last)
			#print("index is :" + " " + str(index1))
			randomtweet = Tweet.objects.filter(classified=False)[index1]

		except:
			randomtweet = "empty"

	

	def phrase_classify(tweetclassed,request):


		target_tweet_id = tweetclassed.target_tweet.tweetid
		target_username = tweetclassed.target_tweet.username
		tweet_text = tweetclassed.text

		global rxn_group
		rxn_group = []

		#first, for each sentiment clicked in request, make a TweetClass record

		for key in request.POST:
			value = request.POST[key]
			print(value)
			if value in reactsetflat:
				rxn_group.append(value)

		print("********REACTION GROUP")
		print(rxn_group)

		tmc = TweetClass(tweet=tweetclassed)
		tmc.save()

		tmc = TweetClass.objects.get(tweet=tweetclassed)

		for rxn in rxn_group: #rxn_group will be a block of reactions

			setattr(tmc,rxn,1) #sets the field of the model to 1, signifying
			tmc.save()



		tweet_text = tweet_text.split()
		tweet_len = len(tweet_text)
		x = tweet_len
		phrase_set = []
		tslice_begin = 0 
		tslice_length = 1
		phrase_limit = 3

		while tslice_length <= phrase_limit:
		#while tslice_length <= tweet_len: #have full tweet recorded here
			while (tslice_begin + tslice_length) <= tweet_len:
				phrase = ''
				
				for wordle in tweet_text[tslice_begin:tslice_begin + tslice_length]:
					phrase = phrase + wordle + " "

				phrase = phrase[0:len(phrase)-1]
				#print(phrase)
				print(phrase.encode('ascii', 'ignore'))
				phrase_set.append(phrase)
			
				tslice_begin = tslice_begin + 1

			tslice_begin = 0
			
			tslice_length = tslice_length + 1

		print("")

		phrase_set = list(set(phrase_set))

		for phrase in phrase_set:
			#for rxn in rxn_group:

			try: 
				existing_pb = PhraseBit.objects.get(text=phrase,target_user=target_username,target_tweet=tweetclassed.target_tweet)

				existing_pb.value = existing_pb.value + 1
				existing_pb.save()

				for rxn in rxn_group:
					
					setattr(existing_pb,rxn, getattr(existing_pb,rxn) + 1)
					existing_pb.save()


			except:
				#print("Tweet ALL pb created")

				pb = PhraseBit(text=phrase, target_user=target_username,target_tweet=tweetclassed.target_tweet,value=1) #first pb
				pb.save()

				pb = PhraseBit.objects.get(text=phrase, target_user=target_username,target_tweet=tweetclassed.target_tweet)
				
				for rxn in rxn_group:
					#print("***" + str(rxn))
					setattr(pb,rxn,1) #since its the first one to count toward this phrasebit
					pb.save()


		print("Number of classified phrases = " + str(len(phrase_set)))




	if 'rxn_group' in request.POST:
		print("Successful post!")
		print(request.POST)

		global randomtweet

		#print(tweetid)

		randomtweet = Tweet.objects.get(tweetid=tweetid)

		phrase_classify(randomtweet,request)

		randomtweet.classified = True
		randomtweet.save()
		grab_random()


	else:

		try:
			grab_random()

		except:
			randomtweet = 'empty'


	context = {'randomtweet': randomtweet,'reactions':reactset}

	return render(request, 'Sententio/Classifier_Portal.html', context)


#CLassify incoming tweets using phrase bits

def Customer_Portal(request):
	bar_factor = 0.8
	print(request)
	random_index = 0
	t_tweet_all = Target_Tweet.objects.all()
	random_index = random.randint(0,len(t_tweet_all)-1)
	t_tweet = t_tweet_all[random_index] #test tweet
	t_stats = Target_Tweet_Stat.objects.filter(target_tweet=t_tweet)



	reactions = Reaction.objects.filter(polarity="POS")
	
	reactset = []
	reactset1 = []
	reactset2 = []

	reactsetflat = []

	for reaction in reactions:
		reactblock = [reaction.name,reaction.opposite.name]
		reactset1.append(reactblock)
	
	reactions = Reaction.objects.filter(polarity="NEU")

	for reaction in reactions:
		reactblock = [reaction.name,reaction.opposite.name]
		reactset2.append(reactblock)

	for reactblock in reactset2:
		if [reactblock[1],reactblock[0]] in reactset2:
			reactset2.remove(reactblock)

	reactset.extend(reactset1)
	reactset.extend(reactset2)

	reactions = Reaction.objects.all()

	for reaction in reactions:
		reactsetflat.append(reaction.name)

	hello_world = "HELLOOOO WORLD."

	t_stat_set = {}

	for react in reactsetflat:
		for t_stat in t_stats:
			#print(str(t_stat.reaction.name) + " | " + str(react))
			if t_stat.reaction.name == react:
				t_stat_set[react] = {'all': round(t_stat.avg_allval_mod,1), 'user': round(t_stat.avg_userval_mod,1),'tweet': round(t_stat.avg_tweetval_mod,1)} #this adds the three levels for that reactions
			
			else:
				print("No match for " + react)
				

	reactset1_rev = []
	reactset2_rev = []

	for item in reactset1:
		reactblock = [item[1],item[0]]
		reactset1_rev.append(reactblock)

	for item in reactset2:
		reactblock = [item[1],item[0]]
		reactset2_rev.append(reactblock)

	reactpairs1 = []
	reactpairs2 = []

	for reactpair in reactset1_rev:
		reactpair_val = [0,0]
		for t_stat in t_stats:
			if t_stat.reaction.name == reactpair[0]:
				reactpair_val[0] = t_stat.avg_allval_mod

			if t_stat.reaction.name == reactpair[1]:
				reactpair_val[1] = t_stat.avg_allval_mod

		reactpairs1.append(reactpair_val)

	for reactpair in reactset2_rev:
		reactpair_val = [0,0]
		for t_stat in t_stats:
			if t_stat.reaction.name == reactpair[0]:
				reactpair_val[0] = t_stat.avg_allval_mod

			if t_stat.reaction.name == reactpair[1]:
				reactpair_val[1] = t_stat.avg_allval_mod

		reactpairs2.append(reactpair_val)

	reactpairs_diff1 = []
	reactpairs_diff2 = []

	for item in reactpairs1:
		reactpairs_diff1.append(abs(item[1]+item[0]))

	for item in reactpairs2:
		reactpairs_diff2.append(abs(item[1]+item[0]))

	print(t_stat_set)

	reactpairs_and_diff1 = []
	reactpairs_and_diff2 = []

	n = 0

	for item in reactset1_rev:
		block = [item,reactpairs_diff1[n]]
		reactpairs_and_diff1.append(block)
		n = n+1

	n = 0

	for item in reactset2_rev:
		block = [item,reactpairs_diff2[n]]
		reactpairs_and_diff2.append(block)
		n = n+1

	rpad_temp1 = []
	rpad_temp2 = []
	#rpad_temp.append(reactpairs_and_diff[0])

	n = 1

	while n < len(reactpairs_and_diff1):
		rpad_temp1.append(reactpairs_and_diff1[n])
		n = n+1

	n = 0

	while n < len(reactpairs_and_diff2):
		rpad_temp2.append(reactpairs_and_diff2[n])
		n = n+1

	rpad_temp1.sort(key=lambda x: x[1], reverse=True)
	rpad_temp2.sort(key=lambda x: x[1], reverse=False)
	#unsorted_list.sort(key=lambda x: x[3])

	rpad_ord1 = []
	rpad_ord2 = []

	rpad_ord1.append(reactpairs_and_diff1[0])

	for item in rpad_temp1:
		rpad_ord1.append(item)

	for item in rpad_temp2:
		rpad_ord2.append(item)

	reactset1 = [] #clear the original react set
	reactset2 = []

	for item in rpad_ord1:
		reactset1.append(item[0])

	for item in rpad_ord2:
		reactset2.append(item[0])


	context = {'t_tweet':t_tweet,'t_stat_set':t_stat_set,'reactset':reactset,'reactset1':reactset1,'reactset2':reactset2,'bar_factor':bar_factor}

	return render(request, 'Sententio/Customer_Portal.html', context)

def TweetModeler(tweet): #takes an incoming tweet 
	
	#tweets = Tweet.objects.filter(modeled=False)

	rxn_group = []

	rxns = Reaction.objects.all()

	check_block = []

	for rxn in rxns:
		rxn_group.append(rxn.name)

	target_tweet_id = tweet.target_tweet.tweetid
	target_username = tweet.target_tweet.username
	tweet_text = tweet.text

	tweet_text = tweet_text.split()
	tweet_len = len(tweet_text)
	x = tweet_len
	phrase_set = []
	tslice_begin = 0 
	tslice_length = 1
	phrase_limit = 3

	while tslice_length <= phrase_limit:
	#while tslice_length <= tweet_len: #have full tweet recorded here
		while (tslice_begin + tslice_length) <= tweet_len:
			phrase = ''
			
			for wordle in tweet_text[tslice_begin:tslice_begin + tslice_length]:
				phrase = phrase + wordle + " "

			phrase = phrase[0:len(phrase)-1]
			#print(phrase)
			#print(phrase.encode('ascii', 'ignore'))
			phrase_set.append(phrase)
		
			tslice_begin = tslice_begin + 1

		tslice_begin = 0
		
		tslice_length = tslice_length + 1

	print("")

	phrase_set = list(set(phrase_set))

	phrase_value_block = []

	phrase_set_length = len(phrase_set)

	phrase_match_count_ALL = 0
	phrase_match_count_USR = 0
	phrase_match_count_TWT = 0

	tm_ALL = TweetMod(tweet=tweet,level="ALL")
	tm_ALL.save() #creates a new TweetMod object with no values
	tm_ALL = TweetMod.objects.get(tweet=tweet,level="ALL")

	tm_USR = TweetMod(tweet=tweet,level="USR")
	tm_USR.save() #creates a new TweetMod object with no values
	tm_USR = TweetMod.objects.get(tweet=tweet,level="USR")

	tm_TWT = TweetMod(tweet=tweet,level="TWT")
	tm_TWT.save() #creates a new TweetMod object with no values
	tm_TWT = TweetMod.objects.get(tweet=tweet,level="TWT")

	rxn_bit_block_ALL = {}
	rxn_bit_block_USR = {}
	rxn_bit_block_TWT = {}

	for rxn in rxn_group:
		rxn_bit_block_ALL[rxn] = [] #this is a list within the library of rxn_bit_block; averages will come out of it
		rxn_bit_block_USR[rxn] = []
		rxn_bit_block_TWT[rxn] = []

	for phrase in phrase_set:
		#for rxn in rxn_group:
		print(phrase)

		try: 
			existing_pbs = PhraseBit.objects.filter(text=phrase)

			if existing_pbs: #this can be a QuerySet returning no results now

				phrase_match_count_ALL = phrase_match_count_ALL + 1
				

				for rxn in rxn_group:

					bit_block = []

					for existing_pb in existing_pbs:
						bit_block.append(getattr(existing_pb,rxn)/getattr(existing_pb,'value'))
					

					bit_block_avg = np.mean(bit_block)
					
					rxn_bit_block_ALL[rxn].append(bit_block_avg)
					

			else:
				print("ALL ELSE 1")

		except:
			print("No match in ALL set for: " + str(phrase))

	# for rxn in rxn_group:

	# 	setattr(tm_ALL,rxn,np.mean(rxn_bit_block_ALL[rxn]))

	# tm_ALL.save()


		rxn_bit_block = {}

		# for rxn in rxn_group:
		# 	rxn_bit_block[rxn] = []

		try: 
			existing_pbs = PhraseBit.objects.filter(text=phrase, target_user=target_username)

			if existing_pbs: #this can be a QuerySet returning no results now

				phrase_match_count_USR = phrase_match_count_USR + 1
				

				for rxn in rxn_group:

					bit_block = []

					for existing_pb in existing_pbs:
						bit_block.append(getattr(existing_pb,rxn)/getattr(existing_pb,'value'))

					bit_block_avg = np.mean(bit_block)
					
					rxn_bit_block_USR[rxn].append(bit_block_avg)
					



				#print("Match USR: " + str(phrase))

			else:
				print("USR ELSE 1")
		except:
			print("No match in USR set for: " + str(phrase))


		# for rxn in rxn_group:

		# 	setattr(tm_USR,rxn,np.mean(rxn_bit_block_USR[rxn]))

		# tm_USR.save() 

		# for rxn in rxn_group:
		# 	rxn_bit_block[rxn] = []

		try: 
			existing_pbs = PhraseBit.objects.filter(text=phrase, target_tweet=tweet.target_tweet)

			if existing_pbs: #this can be a QuerySet returning no results now

				phrase_match_count_TWT = phrase_match_count_TWT + 1
				

				for rxn in rxn_group:

					bit_block = []

					for existing_pb in existing_pbs:
						bit_block.append(getattr(existing_pb,rxn)/getattr(existing_pb,'value'))

					bit_block_avg = np.mean(bit_block)
					
					rxn_bit_block_TWT[rxn].append(bit_block_avg)
					


				#print("Match TWT: " + str(phrase))

			else:
				print("TWT ELSE 1")
		except:
			print("No match in TWT set for: " + str(phrase))

	for rxn in rxn_group:

		setattr(tm_ALL,rxn,np.mean(rxn_bit_block_ALL[rxn]))
		setattr(tm_USR,rxn,np.mean(rxn_bit_block_USR[rxn]))
		setattr(tm_TWT,rxn,np.mean(rxn_bit_block_TWT[rxn]))

		# tm_TWT.save() 

	tm_ALL.prcnt_pb = round((phrase_match_count_ALL/phrase_set_length)*100,3)
	tm_ALL.save()

	tm_USR.prcnt_pb = round((phrase_match_count_USR/phrase_set_length)*100,3)
	tm_USR.save()

	tm_TWT.prcnt_pb = round((phrase_match_count_TWT/phrase_set_length)*100,3)
	tm_TWT.save()

	tweet.modeled = True
	tweet.save()

		#

def make_target_tweet(tweetid,text,username):
	target_tweet = Target_Tweet(tweetid=tweetid,text=text,username=Username.objects.get(username=username))
	target_tweet.save() #this is a dummy tt model



def targetTweetModeler(target_tweet):

	rxn_group = []

	rxns = Reaction.objects.all()

	for rxn in rxns:
		rxn_group.append(rxn.name)

	tweets = Tweet.objects.filter(target_tweet=target_tweet)
	tweetcount = tweets.count()

	avg_prcnt_pb_all_set = [] 
	avg_prcnt_pb_user_set = [] 
	avg_prcnt_pb_tweet_set = [] 



	for tweet in tweets:

		tweet_ALL = TweetMod.objects.get(tweet=tweet, level="ALL")
		tweet_USR = TweetMod.objects.get(tweet=tweet, level="USR")
		tweet_TWT = TweetMod.objects.get(tweet=tweet, level="TWT")

		avg_prcnt_pb_all_set.append(tweet_ALL.prcnt_pb)
		avg_prcnt_pb_user_set.append(tweet_USR.prcnt_pb)
		avg_prcnt_pb_tweet_set.append(tweet_TWT.prcnt_pb)

		# avg_est_err_all_set.append(tweet_ALL.est_err)
		# avg_est_err_user_set.append(tweet_USR.est_err)
		# avg_est_err_tweet_set.append(tweet_TWT.est_err)

	avg_prcnt_pb_all = np.mean(avg_prcnt_pb_all_set)
	avg_prcnt_pb_user = np.mean(avg_prcnt_pb_user_set)
	avg_prcnt_pb_tweet = np.mean(avg_prcnt_pb_tweet_set)

	# avg_est_err_all = np.mean(avg_est_err_all_set)
	# avg_est_err_user = np.mean(avg_est_err_user_set)
	# avg_est_err_tweet = np.mean(avg_est_err_tweet_set)

	for rxn in rxn_group:

		tt = Target_Tweet_Stat(target_tweet=target_tweet) #creates target tweet model
		tt.reaction = Reaction.objects.get(name=rxn)
		tt.tweet_count = tweetcount #just a count of the number of tweets in the system that are replying to this and which have been modeled

		react_count_all = 0
		react_count_user = 0
		react_count_tweet = 0

		avg_allval_mod_set = [] 
		avg_userval_mod_set = []
		avg_tweetval_mod_set = []

		avg_est_err_all_set = []
		avg_est_err_user_set = []
		avg_est_err_tweet_set = []
		

		for tweet in tweets:

			tweet_ALL = TweetMod.objects.get(tweet=tweet, level="ALL")
			tweet_USR = TweetMod.objects.get(tweet=tweet, level="USR")
			tweet_TWT = TweetMod.objects.get(tweet=tweet, level="TWT")
			
			if getattr(tweet_ALL,rxn) != 0:
				react_count_all = react_count_all + 1

			if getattr(tweet_USR,rxn) != 0:
				react_count_user = react_count_user + 1

			if getattr(tweet_USR,rxn) != 0:
				react_count_tweet = react_count_tweet + 1 

			avg_allval_mod_set.append(getattr(tweet_ALL,rxn)) 
			avg_userval_mod_set.append(getattr(tweet_USR,rxn))
			avg_tweetval_mod_set.append(getattr(tweet_TWT,rxn))

			avg_est_err_all_set.append(getattr(tweet_ALL,str(rxn)+'_err')) 
			avg_est_err_user_set.append(getattr(tweet_USR,str(rxn)+'_err'))
			avg_est_err_tweet_set.append(getattr(tweet_TWT,str(rxn)+'_err'))

		avg_allval_mod = np.mean(avg_allval_mod_set)
		avg_userval_mod = np.mean(avg_userval_mod_set)
		avg_tweetval_mod = np.mean(avg_tweetval_mod_set)

		avg_err_all = np.mean(avg_est_err_all_set)
		avg_err_user = np.mean(avg_est_err_user_set)
		avg_err_tweet = np.mean(avg_est_err_tweet_set)

		tt.avg_allval_mod = avg_allval_mod*100
		tt.avg_userval_mod = avg_userval_mod*100
		tt.avg_tweetval_mod = avg_tweetval_mod*100

		tt.avg_err_all = avg_err_all*100
		tt.avg_err_user = avg_err_user*100
		tt.avg_err_tweet = avg_err_tweet*100

		tt.reaction_prcnt_all = 100*react_count_all/tweetcount
		tt.reaction_prcnt_user = 100*react_count_user/tweetcount
		tt.reaction_prcnt_tweet = 100*react_count_tweet/tweetcount

		tt.avg_prcnt_pb_all = avg_prcnt_pb_all
		tt.avg_prcnt_pb_user = avg_prcnt_pb_user
		tt.avg_prcnt_pb_tweet = avg_prcnt_pb_tweet


		#avg_accuracy_all = models.FloatField(default=0) #avg modeled score (all) versus avg classified value, shown as +/- error 
		#avg_accuracy_user = models.FloatField(default=0) #avg modeled score (username) versus avg classified value, shown as +/- error
		#avg_accuracy_tweet = models.FloatField(default=0) #avg modeled score (tweet) versus avg classified value, shown as +/- error

		#avg_val_class = models.FloatField(default=0) #average pb value of classified tweets for reaction
		#all_prcnt_classed = models.FloatField(default=0) #count classed reactions divided by count modeled reactions

		if react_count_all > 0:
			tt.save() #this saves space, so you can assume the value is zero

def error_estimate(tweet): #adds the estimated average error to the TweetMod classes
	levels = ["ALL","USR","TWT"]

	rxn_group = []

	rxns = Reaction.objects.all()

	for rxn in rxns:
		rxn_group.append(rxn.name)

	try:
		tweetclass = TweetClass.objects.get(tweet=tweet)

		for level in levels:
			tweetmod = TweetMod.objects.get(tweet=x,level=level)
			rxn_err_set = []

			for rxn in rxn_group:
				mod_rxn = getattr(tweetmod,rxn)
				#print('mod_rxn ' + str(mod_rxn))
				class_rxn = getattr(tweetclass,rxn)
				#print('class_rxn ' + str(class_rxn))
				abs_error = abs(mod_rxn-class_rxn)

				rxn_err_set.append(abs_error)

				setattr(tweetmod,str(rxn)+'_err',abs_error)
				

			avg_rxn_err = np.mean(rxn_err_set) 
			
			tweetmod.est_err = avg_rxn_err
			print(rxn_err_set)
			print(level)
			print(avg_rxn_err)
			tweetmod.save()

	except:
		print("No matching *Classified* tweet")

# def login_page(request):
# 	username_form 

# 	return render(request, 'Sententio/login.html', context)

def login_redirect(request):
	username = None
	usergroup = None
	if request.user.is_authenticated:
		username = request.user.username
		usergroup = request.user.groups.all()[0]

	context = {}

	if usergroup.name == 'Administrator':
		return redirect('/admin/')

	elif usergroup.name == 'Classifier':
		return redirect('Sententio/Classifier_Portal/')

	elif usergroup.name == 'Reviewer':
		return render(request, 'Sententio/Reviewer_Portal.html', context)

	elif usergroup.name == 'Customer':
		return redirect('Sententio/Customer_Portal')

	else:
		return render(request, 'Sententio/404_page.html', context)

# def Classifier_Portal(request):

# 	context = {}

# 	return render(request, 'Sententio/Classifier_Portal.html',context)