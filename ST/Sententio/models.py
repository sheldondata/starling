from django.db import models

# Create your models here.

class Target_Name(models.Model):
	username = models.CharField(max_length=50) #get max length
	rank = models.IntegerField(default=0)
	followers = models.IntegerField(default=0)
	#userid = models.CharField(max_length=30) #get max length

	def __str__(self):
		return self.username

	class Meta:
		verbose_name = ("Target Name")
		verbose_name_plural = ("Target Names")

class Reaction(models.Model):
	name = models.TextField(max_length=30)
	opposite = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank = True)
	polarity = models.CharField(max_length=3, choices=(("POS","positive"),("NEU","neutral"),("NEG","negative")),default="NEU") #this is whether it is associated with good versus bad feelings

	def __str__(self):
		return self.name

class Target_Tweet(models.Model):
	tweetid = models.CharField(max_length=40)
	text = models.CharField(max_length=300, null=True)
	username = models.ForeignKey(Target_Name,null=True, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.username) + ': ' + str(self.text)

	class Meta:
		verbose_name = ("Target Tweet")
		verbose_name_plural = ("Target Tweets")


class Target_Tweet_Stat(models.Model):

	target_tweet = models.ForeignKey(Target_Tweet, null=True, on_delete=models.CASCADE)
	reaction = models.ForeignKey(Reaction, null=True, on_delete=models.CASCADE)
	
	reaction_prcnt_all = models.FloatField(default=0) #this is the prcnt of reactions where the reaction occured; ALL level
	reaction_prcnt_user = models.FloatField(default=0) #this is the prcnt of reactions where the reaction occured; user level
	reaction_prcnt_tweet = models.FloatField(default=0) #this is the prcnt of reactions where the reaction occured; target tweet level

	tweet_count = models.IntegerField(default=0) #this is the total number of tweets; used for phrasebit accumulation

	avg_allval_mod = models.FloatField(default=0) #average cumulative weighted pb-based score
	avg_prcnt_pb_all = models.FloatField(default=0) #percent of potential phrase bits matched from All tweets
	avg_err_all = models.FloatField(default=0) #avg modeled score (all) versus avg classified value, shown as +/- error 

	avg_userval_mod = models.FloatField(default=0)
	avg_prcnt_pb_user = models.FloatField(default=0) #percent of potential phrase bits matched from only responses to Target Username
	avg_err_user = models.FloatField(default=0) #avg modeled score (username) versus avg classified value, shown as +/- error

	avg_tweetval_mod = models.FloatField(default=0)
	avg_prcnt_pb_tweet = models.FloatField(default=0) #percent of potential phrase bits matched from only responses to Target Tweet
	avg_err_tweet = models.FloatField(default=0) #avg modeled score (username) versus avg classified value, shown as +/- error

	avg_val_class = models.FloatField(default=0) #average value of classified tweets for reaction
	all_prcnt_classed = models.FloatField(default=0) #count classed reactions divided by count modeled reactions


	def __str__(self):
		return "{" + str(self.reaction) + "} " + str(self.target_tweet)

	class Meta:
		verbose_name = ("Target Tweet Stats")
		verbose_name_plural = ("Target Tweets Stats")

class Tweet(models.Model): #holding bin for unmodeled, unclassed tweets
	tweetid = models.CharField(max_length=40)
	text = models.CharField(max_length=300, null=True)
	target_tweet = models.ForeignKey(Target_Tweet, null=True, blank=True, on_delete=models.CASCADE)
	modeled = models.BooleanField(default=False)
	classified = models.BooleanField(default=False)
	
	def __str__(self):
		return self.text

	class Meta:
		verbose_name = ("Tweet")
		verbose_name_plural = ("Tweets")

# 

class PhraseBit(models.Model):
	text = models.CharField(max_length=150)
	#reaction = models.ForeignKey(Reaction, on_delete=models.CASCADE, null=True)
	# source_count = models.IntegerField(default=0) #number of tweets from which this average was drawn. It'll only be specific to reaction tweets to the target tweet; an incoming response tweet 

	target_tweet = models.ForeignKey(Target_Tweet, blank=True, null=True, on_delete=models.CASCADE)
	target_user = models.ForeignKey(Target_Name, null=True, on_delete=models.CASCADE)

	value = models.IntegerField(default=0) #this is the number of tweets for this vote

	positive = models.IntegerField(default=0)
	negative = models.IntegerField(default=0)
	earnest = models.IntegerField(default=0)
	sarcastic = models.IntegerField(default=0)
	hopeful = models.IntegerField(default=0)
	pessimistic = models.IntegerField(default=0)
	excited = models.IntegerField(default=0)
	calm = models.IntegerField(default=0)
	interested = models.IntegerField(default=0)
	bored = models.IntegerField(default=0)
	happy = models.IntegerField(default=0)
	sad = models.IntegerField(default=0)
	cheerful = models.IntegerField(default=0)
	angry = models.IntegerField(default=0)
	satisfied = models.IntegerField(default=0)
	disappointed = models.IntegerField(default=0)
	surprised = models.IntegerField(default=0)
	unsurprised = models.IntegerField(default=0)
	proud = models.IntegerField(default=0)
	embarrassed = models.IntegerField(default=0)
	confident = models.IntegerField(default=0)
	anxious = models.IntegerField(default=0)
	desirous = models.IntegerField(default=0)
	disgusted = models.IntegerField(default=0)
	fauxnaif = models.IntegerField(default=0)
	pompous = models.IntegerField(default=0)
	humanlike = models.IntegerField(default=0)
	botlike = models.IntegerField(default=0)

	class Meta:
		verbose_name = ("Phrase Bit")
		verbose_name_plural = ("Phrase Bits")

	def __str__(self):
		#return str(self.text) + " [" + str(self.reaction.name) +  "] [" + str(self.target_tweet) + "]"
		return str(self.text)

class TweetMod(models.Model):
	tweet = models.ForeignKey(Tweet, blank=True, null=True, on_delete=models.CASCADE)
	#reaction = models.ForeignKey(Reaction, on_delete=models.CASCADE, null=True)
	level = models.CharField(max_length=3,choices=(("ALL","all tweets"),("USR","username"),("TWT","target tweet")), null=True)
	prcnt_pb = models.FloatField(default=0) #this is the % of all phrase bits that were covered

	est_err = models.FloatField(null=True, blank=True)
	
	positive = models.FloatField(default=0)
	negative = models.FloatField(default=0)
	earnest = models.FloatField(default=0)
	sarcastic = models.FloatField(default=0)
	hopeful = models.FloatField(default=0)
	pessimistic = models.FloatField(default=0)
	excited = models.FloatField(default=0)
	calm = models.FloatField(default=0)
	interested = models.FloatField(default=0)
	bored = models.FloatField(default=0)
	happy = models.FloatField(default=0)
	sad = models.FloatField(default=0)
	cheerful = models.FloatField(default=0)
	angry = models.FloatField(default=0)
	satisfied = models.FloatField(default=0)
	disappointed = models.FloatField(default=0)
	surprised = models.FloatField(default=0)
	unsurprised = models.FloatField(default=0)
	proud = models.FloatField(default=0)
	embarrassed = models.FloatField(default=0)
	confident = models.FloatField(default=0)
	anxious = models.FloatField(default=0)
	desirous = models.FloatField(default=0)
	disgusted = models.FloatField(default=0)
	fauxnaif = models.FloatField(default=0)
	pompous = models.FloatField(default=0)
	humanlike = models.FloatField(default=0)
	botlike = models.FloatField(default=0) 

	positive_err = models.FloatField(default=0)
	negative_err = models.FloatField(default=0)
	earnest_err = models.FloatField(default=0)
	sarcastic_err = models.FloatField(default=0)
	hopeful_err = models.FloatField(default=0)
	pessimistic_err = models.FloatField(default=0)
	excited_err = models.FloatField(default=0)
	calm_err = models.FloatField(default=0)
	interested_err = models.FloatField(default=0)
	bored_err = models.FloatField(default=0)
	happy_err = models.FloatField(default=0)
	sad_err = models.FloatField(default=0)
	cheerful_err = models.FloatField(default=0)
	angry_err = models.FloatField(default=0)
	satisfied_err = models.FloatField(default=0)
	disappointed_err = models.FloatField(default=0)
	surprised_err = models.FloatField(default=0)
	unsurprised_err = models.FloatField(default=0)
	proud_err = models.FloatField(default=0)
	embarrassed_err = models.FloatField(default=0)
	confident_err = models.FloatField(default=0)
	anxious_err = models.FloatField(default=0)
	desirous_err = models.FloatField(default=0)
	disgusted_err = models.FloatField(default=0)
	fauxnaif_err = models.FloatField(default=0)
	pompous_err = models.FloatField(default=0)
	humanlike_err = models.FloatField(default=0)
	botlike_err = models.FloatField(default=0) 

	class Meta:
		verbose_name = ("Modeled Tweet")
		verbose_name_plural = ("Modeled Tweets")

	def __str__(self):
		return str(self.tweet) + " [" + str(self.level) + "]"
#then aggregate by all and store, by target_user and store, and 

class TweetClass(models.Model):
	tweet = models.ForeignKey(Tweet, blank=True, null=True, on_delete=models.CASCADE)
	#reaction = models.ForeignKey(Reaction, on_delete=models.CASCADE, null=True)

	positive = models.IntegerField(default=0)
	negative = models.IntegerField(default=0)
	earnest = models.IntegerField(default=0)
	sarcastic = models.IntegerField(default=0)
	hopeful = models.IntegerField(default=0)
	pessimistic = models.IntegerField(default=0)
	excited = models.IntegerField(default=0)
	calm = models.IntegerField(default=0)
	interested = models.IntegerField(default=0)
	bored = models.IntegerField(default=0)
	happy = models.IntegerField(default=0)
	sad = models.IntegerField(default=0)
	cheerful = models.IntegerField(default=0)
	angry = models.IntegerField(default=0)
	satisfied = models.IntegerField(default=0)
	disappointed = models.IntegerField(default=0)
	surprised = models.IntegerField(default=0)
	unsurprised = models.IntegerField(default=0)
	proud = models.IntegerField(default=0)
	embarrassed = models.IntegerField(default=0)
	confident = models.IntegerField(default=0)
	anxious = models.IntegerField(default=0)
	desirous = models.IntegerField(default=0)
	disgusted = models.IntegerField(default=0)
	fauxnaif = models.IntegerField(default=0)
	pompous = models.IntegerField(default=0)
	humanlike = models.IntegerField(default=0)
	botlike = models.IntegerField(default=0)
	#val = models.BooleanField()

	def __str__(self):
		return str(self.tweet) #+ " [" + str(self.reaction) + "] "

	class Meta:
		verbose_name = ("Classified Tweet")
		verbose_name_plural = ("Classified Tweets")



#ARCHIVED

#class TweetModeled(models.Model): #contains modeled assigned values for tweet, but only tweets that have not been classified
# 	tweet = models.ForeignKey(Tweet, null=True, on_delete=models.CASCADE)
# 	prcnt_pb = models.FloatField(default=0) #based on this percent of matched phrase bits
	
# 	positive_negative = models.FloatField(default=0)
# 	earnest_sarcastic = models.FloatField(default=0)
# 	hopeful_pessimistic = models.FloatField(default=0)
# 	excited_calm = models.FloatField(default=0)
# 	interested_bored = models.FloatField(default=0)
# 	happy_sad = models.FloatField(default=0)
# 	cool_angry = models.FloatField(default=0)
# 	satisfied_disappointed = models.FloatField(default=0)
# 	surprised_unsurprised = models.FloatField(default=0)
# 	proud_embarrassed = models.FloatField(default=0)
# 	confident_anxious = models.FloatField(default=0)
# 	desirous_disgusted = models.FloatField(default=0)
# 	fauxnaif_nonfauxnaif = models.FloatField(default=0)
# 	humanlike_botlike = models.FloatField(default=0)
	
# 	def __str__(self):
# 		return self.tweet.text

# 	class Meta:
# 		verbose_name = ("Tweet Modeled")
# 		verbose_name_plural = ("Tweets Modeled")

# class TweetClassed(models.Model):
# 	tweet = models.ForeignKey(Tweet, null=True, on_delete=models.CASCADE)

# 	positive_negative = models.FloatField(default=0)
# 	earnest_sarcastic = models.FloatField(default=0)
# 	hopeful_pessimistic = models.FloatField(default=0)
# 	excited_calm = models.FloatField(default=0)
# 	interested_bored = models.FloatField(default=0)
# 	happy_sad = models.FloatField(default=0)
# 	cool_angry = models.FloatField(default=0)
# 	satisfied_disappointed = models.FloatField(default=0)
# 	surprised_unsurprised = models.FloatField(default=0)
# 	proud_embarrassed = models.FloatField(default=0)
# 	confident_anxious = models.FloatField(default=0)
# 	desirous_disgusted = models.FloatField(default=0)
# 	fauxnaif_nonfauxnaif = models.FloatField(default=0)
# 	humanlike_botlike = models.FloatField(default=0)
	
# 	def __str__(self):
# 		return self.tweet.text

# 	class Meta:
# 		verbose_name = ("Tweet Classified")
# 		verbose_name_plural = ("Tweets Classified")



# class PhraseBit_ALL(models.Model):
# 	text = models.CharField(max_length=150)
# 	#target_user = models.ForeignKey(Target_Name,blank=True, null=True,on_delete=models.CASCADE)

# 	source_count = models.IntegerField(default=0) #representing entire dbase of tweets

# 	positive_negative = models.FloatField(default=0)
# 	earnest_sarcastic = models.FloatField(default=0)
# 	hopeful_pessimistic = models.FloatField(default=0)
# 	excited_calm = models.FloatField(default=0)
# 	interested_bored = models.FloatField(default=0)
# 	happy_sad = models.FloatField(default=0)
# 	cool_angry = models.FloatField(default=0)
# 	satisfied_disappointed = models.FloatField(default=0)
# 	surprised_unsurprised = models.FloatField(default=0)
# 	proud_embarrassed = models.FloatField(default=0)
# 	confident_anxious = models.FloatField(default=0)
# 	desirous_disgusted = models.FloatField(default=0)
# 	fauxnaif_nonfauxnaif = models.FloatField(default=0)
# 	humanlike_botlike = models.FloatField(default=0)

# 	def __str__(self):
# 		return self.text

# 	class Meta:
# 		verbose_name = ("Phrase Bit ALL")
# 		verbose_name_plural = ("Phrase Bits ALL")

# class PhraseBit_USERNAME(models.Model):
# 	text = models.CharField(max_length=150)
# 	username = models.ForeignKey(Target_Name,blank=True, null=True,on_delete=models.CASCADE)
# 	source_count = models.IntegerField(default=0) #representing only target tweets from username

# 	positive_negative = models.FloatField(default=0)
# 	earnest_sarcastic = models.FloatField(default=0)
# 	hopeful_pessimistic = models.FloatField(default=0)
# 	excited_calm = models.FloatField(default=0)
# 	interested_bored = models.FloatField(default=0)
# 	happy_sad = models.FloatField(default=0)
# 	cool_angry = models.FloatField(default=0)
# 	satisfied_disappointed = models.FloatField(default=0)
# 	surprised_unsurprised = models.FloatField(default=0)
# 	proud_embarrassed = models.FloatField(default=0)
# 	confident_anxious = models.FloatField(default=0)
# 	desirous_disgusted = models.FloatField(default=0)
# 	fauxnaif_nonfauxnaif = models.FloatField(default=0)
# 	humanlike_botlike = models.FloatField(default=0)

# 	def __str__(self):
# 		return self.text

# 	class Meta:
# 		verbose_name = ("Phrase Bit USERNAME")
# 		verbose_name_plural = ("Phrase Bits USERNAME")

# class PhraseBit_TWEET(models.Model):
# 	text = models.CharField(max_length=150)
	
# 	target_tweet = models.ForeignKey(Target_Tweet, blank=True, null=True, on_delete=models.CASCADE)
# 	source_count = models.IntegerField(default=0) #representing only only the specific target tweet

# 	positive_negative = models.FloatField(default=0)
# 	earnest_sarcastic = models.FloatField(default=0)
# 	hopeful_pessimistic = models.FloatField(default=0)
# 	excited_calm = models.FloatField(default=0)
# 	interested_bored = models.FloatField(default=0)
# 	happy_sad = models.FloatField(default=0)
# 	cool_angry = models.FloatField(default=0)
# 	satisfied_disappointed = models.FloatField(default=0)
# 	surprised_unsurprised = models.FloatField(default=0)
# 	proud_embarrassed = models.FloatField(default=0)
# 	confident_anxious = models.FloatField(default=0)
# 	desirous_disgusted = models.FloatField(default=0)
# 	fauxnaif_nonfauxnaif = models.FloatField(default=0)
# 	humanlike_botlike = models.FloatField(default=0)

# 	def __str__(self):
# 		return self.text

# 	class Meta:
# 		verbose_name = ("Phrase Bit TWEET")
# 		verbose_name_plural = ("Phrase Bits TWEET")

