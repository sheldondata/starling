from django.contrib import admin

from Sententio.models import Target_Name, Target_Tweet, Tweet, Reaction, PhraseBit, TweetMod, TweetClass, Target_Tweet_Stat

admin.site.register(Target_Tweet)
admin.site.register(Target_Name)
admin.site.register(Tweet)

admin.site.register(Reaction)

admin.site.register(TweetMod)
admin.site.register(TweetClass)
admin.site.register(Target_Tweet_Stat)

class PhraseBitAdmin(admin.ModelAdmin):  
  #list_display = ('name', 'view_homepage_link', 'created_at', 'updated_at')
  ordering = ('text',) # A negative sign indicate descendent order
  search_fields = ['text']

admin.site.register(PhraseBit,PhraseBitAdmin)

# Register your models here.
