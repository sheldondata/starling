from . import views
from django.urls import include, path
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from django.contrib import admin
from django.views.generic.base import RedirectView

app_name = 'Sententio'

urlpatterns = [
    #path(r'tweet_classifier/', views.tweet_classifier, name='tweet_classifier'),
    #path(r'murmuration/',views.murmuration, name='murmuration'),
    url(r'login/$',auth_views.login, {'template_name':'logged_out.html'},name='login'),
    url(r'logout/$', auth_views.logout,{'template_name':'logged_out.html'}, name='logout'),
    url(r'login_redirect/$', views.login_redirect, name='login_redirect'),
    url(r'Classifier_Portal/$',views.Classifier_Portal, name='Classifier_Portal'),
    url(r'Customer_Portal/$',views.Customer_Portal, name='Customer_Portal')
]